package br.com.test.siteapecof;

import br.com.siteapecof.entity.Evento;
import br.com.siteapecof.entity.Participante;
import br.com.siteapecof.entity.Usuario;
import br.com.siteapecof.pagseguro.CreateCheckout;

/**
 * Created by Marte on 01/07/2016.
 */
public class PagseguroTeste {

    public static void main(String[] args){
        CreateCheckout checkout = new CreateCheckout();

        Evento evento = new Evento();
        evento.setId(1);
        evento.setDescricao("Evento de Teste");
        evento.setReferencia("REF1234");
        evento.setValor(100.00);
        evento.setTitulo("EventTeste");

        Participante participante = new Participante();
        participante.setNome("Romildo");
        participante.setSnome("Alvez da Costa");
        participante.setEmail("romildo@gmail.com");

        checkout.redirecionarPagseguro(checkout.checkoutDetails(evento,participante));


    }
}
