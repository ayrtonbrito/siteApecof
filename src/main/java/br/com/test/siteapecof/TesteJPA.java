package br.com.test.siteapecof;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.siteapecof.dao.UsuarioDAO;
import br.com.siteapecof.entity.Usuario;

public class TesteJPA {

		public static void main(String[] args) {
			Usuario user = new Usuario();

	        user.setEmail("email@email.com");
	        user.setSnome("Silva");
	        user.setNome("Nome");
	        user.setTelefone("1234567890");
	        user.setSenha("senha");
	        
	        UsuarioDAO usuarioDAO = new UsuarioDAO();
	        
			usuarioDAO.adicionarUsuario(user);
		}
}
