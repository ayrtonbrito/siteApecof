package br.com.siteapecof.dao;

import br.com.siteapecof.entity.Evento;
import br.com.siteapecof.util.JPAUtil;
import org.hibernate.HibernateException;

import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Marte on 04/07/2016.
 */
public class EventoDAO implements Serializable {

    private static final long serialVersionUID = -1532712079924276460L;
    private EntityManager manager;

    public EventoDAO(){this.manager = new JPAUtil().getEntityManager();}


    public boolean atualizarOuCriarEvento(Evento evento){
        try {
            this.manager.getTransaction().begin();
            this.manager.merge(evento);
            this.manager.getTransaction().commit();
            //this.manager.close();
            return true;
        }catch(Exception ex){
            ex.printStackTrace();
            return false;
        }

    }

    public boolean deleteEvento(Evento evento){
        try {
            this.manager.getTransaction().begin();
            this.manager.remove(evento);
            this.manager.getTransaction().commit();
            //this.manager.close();
            return true;
        }catch(Exception ex){
            ex.printStackTrace();
            return false;
        }
    }

    public List<Evento> getAll(){
        return this.manager.createQuery("Select ev from Evento ev",Evento.class).getResultList();
    }



    public List<Evento> listarEventosAtivos(){
        try {
            List<Evento> eventoAtivos;
            eventoAtivos = this.manager.createQuery("select ev from Evento ev where ev.dataFimInscricao > current_timestamp", Evento.class).getResultList();
            return eventoAtivos;
        }catch(Exception ex){
            ex.printStackTrace();
            return null;
        }

    }

    public void closeEntityManager(){
        this.manager.close();
    }






}
