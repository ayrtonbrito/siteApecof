package br.com.siteapecof.dao;

import javax.persistence.EntityManager;

import br.com.siteapecof.entity.Noticia;
import br.com.siteapecof.util.JPAUtil;

import java.util.List;


public class NoticiaDAO {
	private static final long serialVersionUID = 1532712079924276460L;
    private EntityManager manager;
 
    public NoticiaDAO(){
    	this.manager = new JPAUtil().getEntityManager();
    }
	
    public void adicionarNoticia(Noticia noticia){
    	manager.getTransaction().begin();
    	manager.persist(noticia);
    	manager.getTransaction().commit();
    	manager.close();
    }

	public List<Noticia> getAllNoticiasOrderByDate(){

		return manager.createQuery("Select n from Noticia n order by n.dataPostagem asc ").getResultList();
	}

	public void editarNoticia(Noticia noticia){
		manager.getTransaction().begin();
		manager.merge(noticia);
		manager.getTransaction().commit();
		manager.close();
	}
}
