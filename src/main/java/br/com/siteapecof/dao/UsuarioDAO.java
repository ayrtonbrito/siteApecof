package br.com.siteapecof.dao;

import br.com.siteapecof.entity.Usuario;
import br.com.siteapecof.util.JPAUtil;
import org.hibernate.HibernateException;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

/**
 * Created by Marte on 25/06/2016.
 */

public class UsuarioDAO {

	private static final long serialVersionUID = 1532712079924276460L;
	private EntityManager manager;

	public UsuarioDAO() {

		this.manager = new JPAUtil().getEntityManager();

	}

	public void adicionarUsuario(Usuario usuario) {
		this.manager.getTransaction().begin();
		manager.merge(usuario);
		manager.getTransaction().commit();
	}

	public void removerUsuario(Usuario usuario) {
		manager.remove(usuario);
	}

	public List<Usuario> listarDiretores() {

		List<Usuario> query = manager.createQuery("select u from Usuario u", Usuario.class)

				.getResultList();
		return query;
	}

	public Usuario getUsuario(String nomeUsuario, String senha) {

		try {
			Usuario usuario = manager
					.createQuery("SELECT u from Usuario u where u.nome = :name and u.senha = :senha", Usuario.class)
					.setParameter("name", nomeUsuario).setParameter("senha", senha).getSingleResult();

			return usuario;
		} catch (NoResultException e) {
			return null;
		}
	}

	public List<Usuario> getAll() {
		return manager.createQuery("SELECT U FROM Usuario U", Usuario.class).getResultList();
	}

}
