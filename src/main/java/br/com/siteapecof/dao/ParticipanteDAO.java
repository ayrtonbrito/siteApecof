package br.com.siteapecof.dao;

import javax.persistence.EntityManager;

import br.com.siteapecof.entity.Participante;
import br.com.siteapecof.util.JPAUtil;

import java.io.Serializable;

public class ParticipanteDAO implements Serializable {

    private static final long serialVersionUID = 1532712079924276460L;
    private EntityManager manager;
    private Participante participante;

    public ParticipanteDAO() {
        this.manager = new JPAUtil().getEntityManager();
    }

    public void adicionarParticipante(Participante participante) {
        manager.getTransaction().begin();
        manager.persist(participante);
        manager.getTransaction().commit();
        //manager.close();
    }

    public void removerParticipante(Participante participante){
        manager.getTransaction().begin();
        manager.remove(manager.contains(participante) ? participante : manager.merge(participante));
        manager.getTransaction().commit();
    }

    public Participante getPacienteByEmail(String email) {
        participante = this.manager.createQuery(
                "SELECT participante from Participante participante where participante.email = '" +
                        email+"'", Participante.class).getResultList().get(0);

        return participante;
    }

}
