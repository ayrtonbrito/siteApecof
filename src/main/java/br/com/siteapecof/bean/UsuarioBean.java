package br.com.siteapecof.bean;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import br.com.siteapecof.dao.UsuarioDAO;
import br.com.siteapecof.entity.TipoUsuario;
import br.com.siteapecof.entity.Usuario;
import br.com.siteapecof.util.Navigation;
import org.hibernate.JDBCException;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@ManagedBean(name = "usuarioBean")
@SessionScoped
public class UsuarioBean implements Serializable {


    private static final long serialVersionUID = 7428046837474353010L;

    private Usuario usuario = new Usuario();
    private UsuarioDAO usuarioDAO = new UsuarioDAO();
    private List<Usuario> diretores;
    private List<Usuario> usuarios;

    @PostConstruct
    public void init() {
        diretores = new ArrayList<>();
        diretores = usuarioDAO.listarDiretores();
        //FacesContext.getCurrentInstance().getExternalContext().getSession(true);
    }


    public void cadastroUsuario() {

        usuarioDAO.adicionarUsuario(usuario);
        usuario = new Usuario();
    }

    public String editarUsuario(Usuario usuario){
        if(usuario != null){
            usuario = null;
        }
            this.usuario = usuario;

        return Navigation.NOVO_USUARIO;
    }

    public String deletarUsuario(Usuario usuario){
        usuarioDAO.removerUsuario(usuario);
        return Navigation.TODOS_USUARIOS;
    }



    public String logout() {
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("currentUser");
        usuario = null;
        return Navigation.LOGIN_PAGE;
    }

    public List<Usuario> getAllUsuarios() {
        if (usuarios == null) {
            usuarios = new ArrayList<>();
        }
        usuarios = usuarioDAO.getAll();

        return this.usuarios;
    }

    public List<Usuario> getDiretores() {
        return diretores;
    }

    public void setDiretores(List<Usuario> diretores) {
        this.diretores = diretores;
    }

    public UsuarioDAO getUsuarioDAO() {
        return usuarioDAO;
    }

    public void setUsuarioDAO(UsuarioDAO usuarioDAO) {
        this.usuarioDAO = usuarioDAO;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public TipoUsuario[] getTipoUsuarios() {
        return TipoUsuario.values();
    }


}
