package br.com.siteapecof.bean;

import br.com.siteapecof.dao.ParticipanteDAO;
import br.com.siteapecof.entity.Evento;
import br.com.siteapecof.entity.Participante;
import br.com.siteapecof.pagseguro.CreateCheckout;

import javax.faces.application.NavigationHandler;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import java.io.IOException;
import java.io.Serializable;

@ManagedBean(name="participanteBean")
@SessionScoped
public class ParticipanteBean implements Serializable {

	private static final long serialVersionUID = -326427879645231893L;
	private Participante participante = new Participante();
	private ParticipanteDAO participanteDao = new ParticipanteDAO();
	private Evento evento;

	public Participante getParticipante() {
		return participante;
	}

	public void setParticipante(Participante participante) {
		this.participante = participante;
	}

    public Evento getEvento() {
        return evento;
    }

    public void setEvento(Evento evento) {
        this.evento = evento;
    }

    public void cadastrarParticipante(){

		System.out.println("Participante" + participante.getNome());
        participante.setEvento(this.evento);
        System.out.println("Evento" + participante.getEvento().getTitulo());
		participanteDao.adicionarParticipante(participante);
		//participante = new Participante();
		this.redirecionarPagueSeguro(participante);

	}

	private String redirecionarPagueSeguro(Participante participante){
		this.participante = participanteDao.getPacienteByEmail(participante.getEmail());
		String url;
		if(this.participante==null){
			return "cadastroParticipante.xhtml?faces-redirect=true";
		}else{
			CreateCheckout checkout = new CreateCheckout();
			url = checkout.redirecionarPagseguro(checkout.checkoutDetails(this.participante.getEvento(),this.participante));
            redirect(url);
			return null;
	    }
    }

    public String participarEvento(Evento evento){
		if(this.evento !=null){
			this.evento = null;
		}
		this.evento = evento;

        return "/cadastroParticipante.xhtml?faces-redirect=true";
    }



    public void redirect(String url){
        try {
            ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
            externalContext.redirect(url);
        } catch (IOException e) {
            e.printStackTrace();

        }
    }





}
