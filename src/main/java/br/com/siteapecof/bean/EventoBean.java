package br.com.siteapecof.bean;

import br.com.siteapecof.dao.EventoDAO;
import br.com.siteapecof.dao.ParticipanteDAO;
import br.com.siteapecof.entity.Evento;
import br.com.siteapecof.entity.Participante;
import br.com.siteapecof.util.Navigation;
import org.hibernate.HibernateException;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@ManagedBean(name = "eventoBean")
@SessionScoped
public class EventoBean implements Serializable {

    private static final long serialVersionUID = 315437879648231893L;

    private EventoDAO eventoDAO = new EventoDAO();
    private ParticipanteDAO participanteDAO = new ParticipanteDAO();
    private Evento evento = new Evento();
    private List<Evento> eventos;
    private Double valor = new Double(0);

    @PostConstruct
    public void init() {
        if (eventos != null) {
            eventos = null;
        }
        eventos = new ArrayList<>();

    }

    public List<Evento> getAllEventos() {
        if (this.eventos != null) {
            eventos = new ArrayList<>();
        }
        eventos = eventoDAO.getAll();
        return eventos;
    }

    public List<Evento> eventosAtivos() {
        if (this.eventos != null) {
            eventos = new ArrayList<>();
        }
        eventos = eventoDAO.listarEventosAtivos();
        return eventos;
    }

    public String alterarEvento(Evento evento) {
        if (this.evento != null) {
            this.evento = null;
        }
        this.evento = evento;
        this.valor = evento.getValor();
        return Navigation.ALTERAR_EVENTO;
    }

    public String removerEvento(Evento evento) {
        this.eventoDAO.deleteEvento(evento);
        return Navigation.TODOS_EVENTOS;
    }

    public String removerParticipante(Participante participante) {
        this.participanteDAO.removerParticipante(participante);
        return Navigation.ALTERAR_EVENTO;
    }

    public String salvarEvento() {
        try {
            evento.setValor(valor);
            eventoDAO.atualizarOuCriarEvento(evento);
            evento = null;
            return Navigation.TODOS_EVENTOS;

        } catch (HibernateException ex) {
            ex.printStackTrace();
            return Navigation.NOVO_EVENTO;
        }
    }

    public String sobreEvento(Evento evento) {
        if (this.evento != null) {
            this.evento = null;
        }
        this.evento = evento;

        return "/visualizarEvento?faces-redirect=true";
    }


    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Evento getEvento() {
        return evento;
    }

    public void setEvento(Evento evento) {
        this.evento = evento;
    }

    public List<Evento> getEventos() {
        return eventos;
    }
}
