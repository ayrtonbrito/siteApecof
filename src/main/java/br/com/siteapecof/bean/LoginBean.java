package br.com.siteapecof.bean;

import br.com.siteapecof.dao.UsuarioDAO;
import br.com.siteapecof.entity.Usuario;
import br.com.siteapecof.util.Navigation;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import java.io.Serializable;

/**
 * Created by Ayrton de Souza on 21/08/2016.
 */

@ManagedBean(name = "loginBean")
@ViewScoped
public class LoginBean implements Serializable {

    private static final long serialVersionUID = -7528096837474653010L;

    private UsuarioDAO usuarioDAO = new UsuarioDAO();
    private String email;
    private String senha;

    public String login() {
        Usuario usuario = usuarioDAO.getUsuario(this.email,this.senha);
        if (usuario == null) {
            FacesContext.getCurrentInstance().addMessage(
                    null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Usuário não encontrado!",
                            "Erro no Login!"));

            return Navigation.LOGIN_PAGE;
        } else {
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("currentUser", usuario);
            return Navigation.TODOS_EVENTOS;
        }
    }

    public String getEmail() {
        return email;
    }

    public String getSenha() {
        return senha;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
}
