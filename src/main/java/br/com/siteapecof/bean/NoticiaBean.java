package br.com.siteapecof.bean;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import br.com.siteapecof.util.Navigation;
import br.com.siteapecof.util.UploadImagem;
import org.primefaces.event.FileUploadEvent;

import br.com.siteapecof.dao.NoticiaDAO;
import br.com.siteapecof.entity.Noticia;
import org.primefaces.model.UploadedFile;

import java.io.File;
import java.util.List;

@ManagedBean
@SessionScoped
public class NoticiaBean {
	
	private Noticia noticia = new Noticia();
	private NoticiaDAO noticiaDAO = new NoticiaDAO();
    private UploadImagem imagem = new UploadImagem();
	
		
	public void handleFileUpload(FileUploadEvent event) {
            this.imagem.fileUpload(event,".jpg","/noticia/");
            this.noticia.setImagem(this.imagem.getNome());
            FacesMessage message = new FacesMessage("Succesful", event.getFile().getFileName() + " is uploaded.");
            FacesContext.getCurrentInstance().addMessage(null, message);

	}

    public String salvarNoticia(){
        noticiaDAO.adicionarNoticia(noticia);
        this.imagem.gravar();

        this.noticia = new Noticia();
        this.imagem = new UploadImagem();

        return Navigation.TODOAS_NOTICIAS;
    }

    public List<Noticia> getAllNoticias(){
        return  noticiaDAO.getAllNoticiasOrderByDate();
    }



	public Noticia getNoticia() {
		return noticia;
	}

    public void setNoticia(Noticia noticia) {
		this.noticia = noticia;
	}


    public String editarNoticia(Noticia noticia) {
        this.noticia = noticia;
        return Navigation.N0VA_NOTICIA;
    }
}
