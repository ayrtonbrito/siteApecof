package br.com.siteapecof.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class Participante  {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -414134248963458741L;

	@Id
    @GeneratedValue
    private int id;
	
	@Column
	@NotNull(message = "Necessario informar o nome")
    private String nome;
    
    @Column
	@NotNull(message = "Necessario informar Sobrenome")
    private String snome;

    @Column
	@NotNull(message = "Necessario informar um email")
    private String email;

    @Column
	@NotNull(message = "Necessario informar um telefone")
    private String telefone;
    
	@Column
	private String empresa;

	@ManyToOne
    @JoinColumn(name="evento_id")
	private Evento evento;


	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getSnome() {
		return snome;
	}
	public void setSnome(String snome) {
		this.snome = snome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getEmpresa() {
		return empresa;
	}
	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}
	public Evento getEvento() {
		return evento;
	}
	public void setEvento(Evento evento) {
		this.evento = evento;
	}
}
