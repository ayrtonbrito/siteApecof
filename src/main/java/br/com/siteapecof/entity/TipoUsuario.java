package br.com.siteapecof.entity;



public enum TipoUsuario {

	Diretoria("Diretor"),Membro("Membro"),ConselhoFiscal("Membro");

	private final String descricao;

	TipoUsuario(String descricao){
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}
}
