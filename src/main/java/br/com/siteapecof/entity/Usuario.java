package br.com.siteapecof.entity;

import br.com.siteapecof.util.MessageUtil;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Marte on 25/06/2016.
 */
@Entity
public class Usuario implements Serializable {

	private static final long serialVersionUID = -3352918263593692526L;

	@Id
    @GeneratedValue
    private int id;

    @Column(length = 20)
	@NotEmpty(message = MessageUtil.NOME_NOT_NULL)
    private String nome;
    
    @Column(length = 20)
	@NotEmpty(message = MessageUtil.SNOME_NOT_NULL)
    private String snome;

    @Column(unique = true, nullable = false)
	@NotNull(message = MessageUtil.EMAIL_NOT_NULL)
	private String email;

	@Column(nullable=false,length=14)
	@Length(min=14,message="O telefone deve conter {min} caracteres. Ex: (85) 9999-9999")
	@NotEmpty(message= MessageUtil.TELEFONE_NOT_NULL)
	private String telefone;
    
    @Column
	@Length(min= 6, message = "A senha deve conter {min} caracteres.")
	@NotEmpty(message = MessageUtil.SENHA_NOT_NULL)
    private String senha;
    
    @Column
	private String cargo;
	
	@Column
	private String miniCurriculo;
	
	@Column
	private String linkLattes;
	
	@Column
	private String tratamento;
    
	@Column
	@Enumerated(EnumType.STRING)
    private TipoUsuario tipoUsuario; 

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

	public String getSnome() {
		return snome;
	}

	public void setSnome(String snome) {
		this.snome = snome;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public String getMiniCurriculo() {
		return miniCurriculo;
	}

	public void setMiniCurriculo(String miniCurriculo) {
		this.miniCurriculo = miniCurriculo;
	}

	public String getLinkLattes() {
		return linkLattes;
	}

	public void setLinkLattes(String linkLattes) {
		this.linkLattes = linkLattes;
	}

	public String getTratamento() {
		return tratamento;
	}

	public void setTratamento(String tratamento) {
		this.tratamento = tratamento;
	}

	public TipoUsuario getTipoUsuario() {
		return tipoUsuario;
	}

	public void setTipoUsuario(TipoUsuario tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}
	
	
}
