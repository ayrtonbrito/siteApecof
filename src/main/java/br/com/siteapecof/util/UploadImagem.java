package br.com.siteapecof.util;

import org.primefaces.event.FileUploadEvent;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileOutputStream;

/**
 * Created by BRIX on 10/09/2016.
 */
public class UploadImagem {


    private String nome;
    private String caminho;
    private byte[] arquivo;
    private String diretorio;

    public String getRealPath() {
        ExternalContext externalContext =
                FacesContext.getCurrentInstance().getExternalContext();
        HttpServletResponse response =
                (HttpServletResponse) externalContext.getResponse();

        FacesContext aFacesContext = FacesContext.getCurrentInstance();
        ServletContext context =
                (ServletContext) aFacesContext.getExternalContext().getContext();

        return context.getRealPath("/resources/");
    }

    public void fileUpload(FileUploadEvent event, String type, String diretorio) {
        try {
            this.nome = new java.util.Date().getTime() + type;
            this.caminho = getRealPath() + diretorio + getNome();
            this.arquivo = event.getFile().getContents();

            File file = new File(getRealPath() + diretorio);
            file.mkdirs();

        } catch (Exception ex) {
            System.out.println("Erro no upload do arquivo" + ex);
        }
    }

    public void gravar() {

        try {

            FileOutputStream fos;
            fos = new FileOutputStream(this.caminho);
            fos.write(this.arquivo);
            fos.close();

        } catch (Exception ex) {
            System.out.println(ex);
        }

    }

    public String getNome() {
        return nome;
    }
}
