package br.com.siteapecof.util;

/**
 * Created by Marte on 20/07/2016.
 */
public class Navigation {

    //Referente ao Login
    public static final String LOGIN_PAGE = "LoginPage";

    //Referentes a Eventos
    public static final String TODOS_EVENTOS = "TodosEventos";
    public static final String EVENTOS_ATIVOS = "EventosAtivos";
    public static final String NOVO_EVENTO = "NovoEvento";
    public static final String ALTERAR_EVENTO = "AlterarEvento";
    public static final String SOBRE_EVENTO = "SobreEvento";

    //Referentes a Participante
    public static final String NOVO_PARTICIPANTE = "NovoParticipante";
    public static final String ALTERAR_PARTICIPANTE = "AlterarParticipante";

    //Referentes a Noticias
    public static final String TODOAS_NOTICIAS = "TodasNoticias";
    public static final String N0VA_NOTICIA = "NovaNoticia";
    public static final String ALTERAR_NOTICIA = "AlterarNoticia";

    //Referente a Usuario
    public static final String TODOS_USUARIOS = "TodosUsuarios";
    public static final String NOVO_USUARIO = "NovoUsuario";
    public static final String ALTERAR_USUARIO = "AlterarUsuario";
    public static final String TODOS_DIRETORES = "TodosDiretores";


}
