package br.com.siteapecof.util;

/**
 * Created by BRIX on 21/08/2016.
 */
public class MessageUtil {


    private MessageUtil() {

    }

    //Referente a Usuario
    public static final String NOME_NOT_NULL = "Nome, não pode estar vazio";
    public static final String SNOME_NOT_NULL = "Sobrenome, não pode estar vazio";
    public static final String EMAIL_NOT_NULL = "O email não pode ser nulo";
    public static final String TELEFONE_NOT_NULL = "Informe um telefone";
    public static final String SENHA_NOT_NULL = "Informe uma senha";
}
