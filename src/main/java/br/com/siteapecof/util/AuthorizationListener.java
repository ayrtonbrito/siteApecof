package br.com.siteapecof.util;

import br.com.siteapecof.dao.ParticipanteDAO;
import br.com.siteapecof.entity.Participante;
import br.com.siteapecof.entity.Usuario;

import javax.faces.application.NavigationHandler;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.servlet.http.HttpSession;

public class AuthorizationListener implements PhaseListener {

    private FacesContext facesContext;
    private String currentPage;
    private boolean isLoginPage;
    private boolean isParticipacaoPage;
    private ParticipanteDAO participanteDAO;

    public void afterPhase(PhaseEvent event) {
        if (facesContext != null) {
            facesContext = null;
        }

        this.facesContext = event.getFacesContext();
        this.currentPage = facesContext.getViewRoot().getViewId();
        this.isLoginPage = (currentPage.lastIndexOf("/apecof/login.xhtml") > -1);
        //this.isParticipacaoPage = (currentPage.lastIndexOf("/apecof/participacao.xhtml") > -1);

        HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);
        Usuario user = (Usuario) session.getAttribute("currentUser");
        //Participante participante = (Participante) session.getAttribute("participanteEvento");

        //if(isParticipacaoPage && participante != null){
           // participanteDAO.adicionarParticipante(participante);
           // NavigationHandler nh = facesContext.getApplication().getNavigationHandler();
          //  nh.handleNavigation(facesContext, null, "index");
      //  }

        if (!isLoginPage && user == null) {
            NavigationHandler nh = facesContext.getApplication().getNavigationHandler();
            nh.handleNavigation(facesContext, null, "LoginPage");

        }


    }

    public void beforePhase(PhaseEvent event) {

    }

    public PhaseId getPhaseId() {
        return PhaseId.RESTORE_VIEW;
    }
}