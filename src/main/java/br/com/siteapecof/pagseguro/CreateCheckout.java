package br.com.siteapecof.pagseguro;

import br.com.siteapecof.entity.Evento;
import br.com.siteapecof.entity.Participante;
import br.com.uol.pagseguro.domain.Item;
import br.com.uol.pagseguro.domain.Sender;
import br.com.uol.pagseguro.domain.checkout.Checkout;
import br.com.uol.pagseguro.enums.Currency;
import br.com.uol.pagseguro.exception.PagSeguroServiceException;
import br.com.uol.pagseguro.properties.PagSeguroConfig;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Locale;


/**
 * Created by Marte on 01/07/2016.
 */
public class CreateCheckout {

    private Checkout checkout;


    public CreateCheckout() {
        this.checkout = new Checkout();
    }

    public Checkout checkoutDetails(Evento evento,Participante participante) {

        BigDecimal valor = new BigDecimal((String.format(Locale.US, "%.2f", evento.getValor())));

        Item item = new Item("" + evento.getId(), evento.getTitulo(), Integer.valueOf(1), valor);

        Sender sender = new Sender(participante.getNome()+" "+participante.getSnome(),participante.getEmail());

        checkout.addItem(item);

        checkout.setSender(sender);

        checkout.setRedirectURL("http:localhost:8080/siteApecof/eventstatus.xhtml");

        checkout.setCurrency(Currency.BRL);

        checkout.setReference(evento.getTitulo().substring(0, 4) + evento.getId());

        return checkout;
    }

    public String redirecionarPagseguro(Checkout checkout) {
        try {

            Boolean onlyCheckoutCode = false;

            /* Set your account credentials on src/pagseguro-config.properties
             * You can create an payment using an application credential and set an authorizationCode
			 * ApplicationCredentials applicationCredentials = PagSeguroConfig.getApplicationCredentials();
             * applicationCredentials.setAuthorizationCode("your_authorizationCode");
             * String checkoutURL = checkout.register(applicationCredentials), onlyCheckoutCode);
			 */

            String checkoutURL = checkout.register(PagSeguroConfig.getAccountCredentials(), onlyCheckoutCode);

            System.out.println(checkoutURL);

            return checkoutURL;

        } catch (PagSeguroServiceException e) {
            e.printStackTrace();
            System.err.println(e.getMessage());
            System.out.println(e.getErrors());
            return null;
        }

    }

}





